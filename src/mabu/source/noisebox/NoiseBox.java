package mabu.source.noisebox;

import android.app.Activity;
import android.os.Bundle;
import android.widget.TextView;
// import configuration stuff
import android.content.res.Configuration;
// import sound player
import android.media.SoundPool;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.content.Context;

import java.lang.Math;

public class NoiseBox extends Activity implements SensorEventListener {

    SoundPool sp;
    int snd;
    SensorManager sm;
    Sensor s;

    boolean pointing_up;
    boolean pointing_down;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        TextView text = (TextView)findViewById(R.id.my_text);
        text.setText("");

        sp = new SoundPool.Builder().setMaxStreams(1).build();
        snd = sp.load(this, R.raw.sound, 1);

        sm = (SensorManager)getSystemService(Context.SENSOR_SERVICE);
        s = sm.getDefaultSensor(Sensor.TYPE_ORIENTATION);

        sm.registerListener(this, s, sm.SENSOR_DELAY_NORMAL);

        pointing_up = false;
        pointing_down = false;
    }



    @Override
    public void onSensorChanged(SensorEvent event) {

        float v[] = event.values;

        float y = v[1];
        float z = Math.abs(v[2]);

        if (y > 70 && z < 10) {
            pointing_down = true;
            pointing_up = false;
        } else if ( y < -70 && z < 10) {
            pointing_up = true;
        }
        if (pointing_up && pointing_down) {
            sp.play(snd, 1, 1, 0, 0, 1);
            pointing_down = false;
            pointing_up = false;
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
    }
}
