# Debian Android Sound playing application
A very simple android application which will act as a Moo box, but saying "Ça Alors!".

## Status

This project results in an installable APK file, but additional testing is needed.

## How do I use this?

Assuming you are using Debian Buster, install the following packages:
```
~# apt-get install make android-sdk android-sdk-platform-23
```

Then, simply check out the project with git, and run '`make`'.

If everything goes well, you will end up with '`noisebox.apk`' in the root directory of the project, which you can transfer to your phone and install in any way you see fit, for example using '`adb install`'.

On the first run, you will be asked for information to create your signing key. If you're just trying things out, you can leave everything as-is.

## Credits

Project clowned from [Coffee](https://gitlab.com/Matrixcoffee), tons of thanks.

## Author

Mabu

## Contact

Not yet

## License

This software is is licensed under the CC0 license, meaning that to the extent possible under law, the author has waived all copyright and related or neighboring rights to this work.

This software is offered as-is, and the author makes no representations or warranties of any kind concerning the software, express, implied, statutory or otherwise, including without limitation warranties of title, merchantability, fitness for a particular purpose, non infringement, or the absence of latent or other defects, accuracy, or the present or absence of errors, whether or not discoverable, all to the greatest extent permissible under applicable law.

The above is a summary; the full license can be found in the file called [LICENSE](LICENSE), or at [the creative commons website](https://creativecommons.org/publicdomain/zero/1.0/legalcode.txt).
